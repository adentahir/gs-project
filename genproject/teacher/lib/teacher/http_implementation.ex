defmodule Teacher.HttpImplementation do

  def get do
    HTTPoison.get!("https://api.coincap.io/v2/assets/bitcoin")
    |> Map.get(:body)
    |> Jason.decode!()
    |> IO.inspect()
    |> Map.get("data")
  end
end
