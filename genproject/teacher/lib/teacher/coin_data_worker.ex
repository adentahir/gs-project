defmodule Teacher.CoinDataWorker do
  use GenServer
  alias Teacher.CoinCapMethods

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(state) do
    schedule_coin_fetch()
    {:ok, state}
  end

  def handle_info(:coin_fetch, state) do
    price = CoinCapMethods.coin_price()
    IO.inspect(price, label: "Current Bitcoin price is $")
    schedule_coin_fetch()
    {:noreply, Map.put(state, :btc , price)}
  end

  defp schedule_coin_fetch do
    Process.send_after(self(), :coin_fetch, 5_000)
  end
end
