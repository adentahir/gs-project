defmodule Teacher.CoinCapMethods do
  alias Teacher.HttpImplementation
  def coin_price do
    HttpImplementation.get()
    |> Map.get("priceUsd")
    |> String.to_float()
  end
end
